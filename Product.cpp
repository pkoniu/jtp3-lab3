#include "Product.h"


Product::Product(std::string n, float p) {
	this->name = n;
	this->price = p;
}

Product::Product(void) {
	this->name = "N/A";
	this->price = 0.0;
}

std::ostream& operator<<(std::ostream &out, const Product &p) {
	out << "Product name: " << p.name << "\t" 
		<< "Price: " << p.price << std::endl;
	return out;
}