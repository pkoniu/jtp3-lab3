#pragma once
#include "Product.h"

class magazine {
private:
	int amount_in_magazine;
	int magazine_capacity;
	Product *product_array;
public:
	magazine(int c) : magazine_capacity(c), amount_in_magazine(0) { product_array = new Product[magazine_capacity]; }
	magazine(void) : magazine_capacity(5), amount_in_magazine(0) { product_array = new Product[magazine_capacity]; }
	magazine(const magazine&);
	~magazine(void) { amount_in_magazine = 0; delete[] product_array;}
	void add_new_product(Product&);
	int const get_amount_in_magazine() { return amount_in_magazine; }
	Product &operator[](int);
	magazine &operator=(const magazine&);
	friend std::ostream& operator<<(std::ostream &out, const magazine &m);
};

