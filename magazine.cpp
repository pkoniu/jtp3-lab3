#include "magazine.h"


magazine::magazine(const magazine &m) {
	this->amount_in_magazine = m.amount_in_magazine;
	this->magazine_capacity = m.magazine_capacity;
	this->product_array = new Product[this->magazine_capacity];
	for(int i=0; i<this->amount_in_magazine; i++) {
		this->product_array[i] = m.product_array[i];
	}
} 

void magazine::add_new_product(Product &p) {
	this->amount_in_magazine++;

	if(this->amount_in_magazine > this->magazine_capacity) {
		throw "no space left in magazine";
	}

	this->product_array[this->amount_in_magazine - 1].setName(p.getName());
	this->product_array[this->amount_in_magazine - 1].setPrice(p.getPrice());
}

Product& magazine::operator[](int i) {
	if(i >= this->magazine_capacity) {
		throw "cannot access content from magazine";
	} else {
		return this->product_array[i];
	}
}

magazine& magazine::operator=(const magazine& m) {
	this->amount_in_magazine = m.amount_in_magazine;
	this->magazine_capacity = m.magazine_capacity;
	this->product_array = new Product[this->magazine_capacity];
	for(int i=0; i<this->amount_in_magazine; i++) {
		this->product_array[i] = m.product_array[i];
	}
	return *this;
}

std::ostream& operator<<(std::ostream &out, const magazine &m) {
	for(int i=0; i<m.magazine_capacity; i++) {
		out << m.product_array[i];
	}
	return out;
}