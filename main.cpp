#include <iostream>
#include "magazine.h"
#include "Product.h"
using namespace std;

int main() {
	magazine m1(3);
	Product p1("bla", 21.42f);
	Product p2("bul", 0.12f);
	Product p3("hue", 23.24f);
	cout << p1 << endl;
	m1.add_new_product(p1);
	m1.add_new_product(p2);
	m1.add_new_product(p3);
	cout << m1 << endl;

	try {
		cout << m1[1] << endl;
		//cout << m1[6] << endl;
	} catch (const char *s) {
		cout << s << endl;
	}

	magazine m2;
	m2 = m1;
	cout << m2;
	//cout << m2.get_amount_in_magazine() << " " << m1.get_amount_in_magazine() << endl;

	system("pause");
	return 0;
}