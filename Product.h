#pragma once
#include <string>
#include <iostream>

class Product {
private:
	std::string name;
	float price;
public:
	Product(std::string, float);
	Product(void);
	void setPrice(float p) { this->price = p; }
	void setName(std::string n) { this->name = n; }
	float const getPrice() { return this->price; }
	std::string const getName() { return this->name; }
	friend std::ostream& operator<<(std::ostream &out, const Product &p);
};

